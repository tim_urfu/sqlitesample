﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace SQLiteSample
{
    public partial class Form1 : Form
    {
        private List<MaterialTable> lst = new List<MaterialTable>();

        private string ConnectionString = "Data Source=sqliteSample.db; Version=3";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillComboBox();
        }

        private void button_Add_Click(object sender, EventArgs e)
        {
            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
                {
                    conn.Open();

                    SQLiteCommand cmd = new SQLiteCommand("INSERT INTO [Table] ([Name], [Value]) VALUES(@name, @value)", conn);

                    cmd.Parameters.AddWithValue("@name", textBox_Name.Text);
                    cmd.Parameters.AddWithValue("@value", Convert.ToDouble(textBox_Value.Text));
                    cmd.ExecuteNonQuery();
                }

                FillComboBox();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void comboBox_Material_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (comboBox_Material.SelectedValue != null)
                {
                    textBox_kValue.Text = lst.FirstOrDefault(x => x.Id == Convert.ToInt32(comboBox_Material.SelectedValue)).Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка: " + ex.Message);
            }
        }

        private void FillComboBox()
        {
            using (SQLiteConnection conn = new SQLiteConnection(ConnectionString))
            {
                conn.Open();

                SQLiteCommand cmd = new SQLiteCommand(@"SELECT * FROM [Table]", conn);
                SQLiteDataReader sqlReader = cmd.ExecuteReader();

                lst = new List<MaterialTable>();

                while (sqlReader.Read()) // считываем и вносим в лист все параметры
                {
                    lst.Add(new MaterialTable
                    {
                        Id = Convert.ToInt32(sqlReader["Id"]),
                        Name = sqlReader["Name"].ToString(),
                        Value = Convert.ToDouble(sqlReader["Value"])
                    });
                }
            }

            comboBox_Material.DataSource = null;
           
            comboBox_Material.DataSource = lst;
            comboBox_Material.ValueMember = "Id";
            comboBox_Material.DisplayMember = "Name";
            comboBox_Material.SelectedIndex = 0;
        }

        private void spravMaterialsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormSprav form = new FormSprav();
            form.Show();
        }
    }
}

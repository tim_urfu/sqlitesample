﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLiteSample
{
    public partial class FormSprav : Form
    {
        public FormSprav()
        {
            InitializeComponent();
        }

        private void FormSprav_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "dBDataSet.Table". При необходимости она может быть перемещена или удалена.
            this.tableTableAdapter.Fill(this.dBDataSet.Table);

        }

        private void button_Update_Click(object sender, EventArgs e)
        {
            tableTableAdapter.Update(dBDataSet.Table);
            tableTableAdapter.Fill(dBDataSet.Table);

            MessageBox.Show("Данные сохранены.");
        }
    }
}
